import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from "rxjs/operators";
import { Escolaridade } from './escolaridade';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
@Injectable({
  providedIn: 'root'
})
export class EscolaridadeService {
  url = 'https://localhost:44336/api/escolaridades';
  constructor(private http: HttpClient) {}

  obterEscolaridades(): Observable<Escolaridade[]> {
    return this.http.get(`${this.url}/ObterTodasEscolaridades`, httpOptions).pipe(map(res => res.retorno));
  }
}



