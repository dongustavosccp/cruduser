﻿using CrudUser.Api.Domain;
using CrudUser.Api.Domain.Contratos.Repositorio;
using CrudUser.Api.Domain.Contratos.Servico;
using System.Collections.Generic;

namespace CrudUser.Api.Servico
{
    public class EscolaridadeServico : IEscolaridadeServico
    {
        private readonly IEscolaridadeRepositorio _escolaridadeRepositorio;

        public EscolaridadeServico(IEscolaridadeRepositorio escolaridadeRepositorio)
        {
            _escolaridadeRepositorio = escolaridadeRepositorio;
        }

        IEnumerable<Escolaridade> IEscolaridadeServico.ObterTodasEscolaridades()
        {
            return _escolaridadeRepositorio.ObterTodasEscolaridades();
        }
    }
}
