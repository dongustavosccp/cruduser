﻿using CrudUser.Api.Domain;
using CrudUser.Api.Domain.Contratos.DomainNotification;
using CrudUser.Api.Domain.Contratos.Repositorio;
using CrudUser.Api.Domain.Contratos.Servico;
using CrudUser.Api.Domain.DomainNotification;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CrudUser.Api.Servico
{
    public class UsuarioServico : IUsuarioServico
    {
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly INotificador _notificador;

        public UsuarioServico(IUsuarioRepositorio usuarioRepositorio, INotificador notificador)
        {
            _usuarioRepositorio = usuarioRepositorio;
            _notificador = notificador;
        }

        public void ExcluirUsuario(int id)
        {
            _usuarioRepositorio.ExcluirUsuario(id);
        }

        public IEnumerable<UsuarioViewModel> ObterTodosUsuarios()
        {
            IEnumerable<UsuarioViewModel> retorno;
            var lista = _usuarioRepositorio.ObterTodosUsuarios();

            retorno = lista.Select(x => new UsuarioViewModel
            {
                Id = x.Id,
                Nome = x.Nome,
                Sobrenome = x.Sobrenome,
                Email = x.Email,
                DataNascimento = x.DataNascimento,
                Escolaridade = x.Escolaridades.Descricao,
            }
        );

            return retorno;
        }

        public Usuario ObterUsuarioPorId(int id)
        {
            return _usuarioRepositorio.ObterUsuarioPorId(id);
        }

        public void Salvar(Usuario usuario)
        {
            if (ValidarEntrada(usuario))
            {
                _usuarioRepositorio.Salvar(usuario);
            }
        }

        private bool ValidarEntrada(Usuario usuario)
        {
            bool retorno = true;

            if (string.IsNullOrWhiteSpace(usuario.Nome))
            {
                _notificador.AdicionarNotificacao(new Notificacao("O Nome é obrigatório"));
            }

            if (string.IsNullOrWhiteSpace(usuario.Sobrenome))
            {
                _notificador.AdicionarNotificacao(new Notificacao("O Sobrenome é obrigatório"));
            }

            if (string.IsNullOrWhiteSpace(usuario.Email))
            {
                _notificador.AdicionarNotificacao(new Notificacao("O E-mail é obrigatório"));
            }

            if (usuario.DataNascimento > DateTime.Now || usuario.DataNascimento.Equals(DateTime.MinValue))
            {
                _notificador.AdicionarNotificacao(new Notificacao("A Data de nascimento deve ser válida e menor que a data atual"));
            }

            return retorno;
        }
    }
}