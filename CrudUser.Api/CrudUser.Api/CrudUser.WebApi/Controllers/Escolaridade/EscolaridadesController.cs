﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudUser.Api.Domain;
using CrudUser.Api.Domain.Contratos.DomainNotification;
using CrudUser.Api.Domain.Contratos.Servico;
using CrudUser.Api.WebApi.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace CrudUser.Api.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EscolaridadesController : BaseController<IEnumerable<Escolaridade>>
    {
        private readonly IEscolaridadeServico _escolaridadeServico;
        private readonly IConfiguration _configuration;

        public EscolaridadesController(IEscolaridadeServico escolaridadeServico, IConfiguration configuration, INotificador notificador) : base(notificador)
        {
            _escolaridadeServico = escolaridadeServico;
            _configuration = configuration;
        }

        [HttpGet("ObterTodasEscolaridades")]
        public ActionResult<IEnumerable<Escolaridade>> ObterTodasEscolaridades()
        {
            IEnumerable<Escolaridade> retorno = null;

            try
            {
                retorno = _escolaridadeServico.ObterTodasEscolaridades();
            }
            catch (System.Exception)
            {
                NotificarErro("Erro ao consultar a lista de usuários");
            }

            return CustomResponse(retorno);
        }
    }
}