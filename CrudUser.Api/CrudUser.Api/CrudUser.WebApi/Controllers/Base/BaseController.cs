﻿using CrudUser.Api.Domain.Contratos.DomainNotification;
using CrudUser.Api.Domain.DomainNotification;
using CrudUser.Api.Domain.Resultado;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace CrudUser.Api.WebApi.Controllers.Base
{
    public class BaseController<T> : ControllerBase
    {
        private readonly INotificador _notificador;

        protected BaseController(INotificador notificador)
        {
            _notificador = notificador;
        }

        protected bool OperacaoValida()
        {
            return !_notificador.TemNotificacao();
        }

        protected ActionResult CustomResponse(T result)
        {
            if (OperacaoValida())
            {
                var retorno = new ResponseResult<T> { Sucesso = true, Retorno = result };

                switch (HttpContext.Request.Method)
                {
                    case "GET":
                        return Ok(retorno);
                    case "POST":
                        return Created(string.Empty, retorno);
                    default:
                        return Ok(retorno);
                }
            }

            return BadRequest(new ResponseResult<T>
            {
                Sucesso = false,
                Retorno = { },
                Erros = _notificador.ObterNotificacoes().Select(n => n.Mensagem)
            });

        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid) NotificarErroModelInvalida(modelState);
            return CustomResponse(null);
        }

        protected void NotificarErroModelInvalida(ModelStateDictionary modelState)
        {
            var erros = modelState.Values.SelectMany(e => e.Errors);

            foreach (var erro in erros)
            {
                var errorMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
                NotificarErro(errorMsg);
            }
        }

        protected void NotificarErro(string mensagem)
        {
            _notificador.AdicionarNotificacao(new Notificacao(mensagem));
        }
    }
}
