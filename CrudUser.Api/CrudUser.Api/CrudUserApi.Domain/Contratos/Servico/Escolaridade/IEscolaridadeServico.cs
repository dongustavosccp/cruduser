﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudUser.Api.Domain.Contratos.Servico
{
    public interface IEscolaridadeServico
    {
        IEnumerable<Escolaridade> ObterTodasEscolaridades();
    }
}
