﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudUser.Api.Domain.Resultado
{
    public class ResponseResult<T>
    {
        public bool Sucesso { get; set; }
        public T Retorno { get; set; }
        public IEnumerable<string> Erros { get; set; }
    }
}
