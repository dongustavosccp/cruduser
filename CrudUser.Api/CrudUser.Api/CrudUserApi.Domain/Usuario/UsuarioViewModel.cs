﻿using System;
using System.Collections.Generic;
using CrudUser.Api.Domain;

namespace CrudUser.Api.Domain
{
    public class UsuarioViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Escolaridade { get; set; }
    }
}
